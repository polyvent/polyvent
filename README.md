# [PolyVent - the global ventilator design formula](https://covid-eic.easme-web.eu/solution/polyvent-global-ventilator-design-formula/about)

PolyVent is a medical ventilator design formula for global production scenarios, which aims to provide needed functionality, flexibility and adaptability based on the regional production and supply chain capabilities.

### Quick links
- [List of all issues (discussions, questions, news etc)](https://gitlab.com/polyvent/polyvent/-/issues).
- [Start new discussion issue](https://gitlab.com/polyvent/polyvent/-/issues/new).
- [Newcomers - ask questions here](https://gitlab.com/polyvent/polyvent/-/issues/38).
- [Access to this site](Access.md).

Our Wiki: https://gitlab.com/polyvent/polyvent/-/wikis/home

### Partner projects
- [Project VentOS](https://gitlab.com/project-ventos/ventos) (moved from the [old repository](https://gitlab.com/bencoombs/ventos)).

<iframe src="https://discordapp.com/widget?id=692438253441122396&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>

### New Team Members - Welcome!
- Welcome to the team! Please feel free to [introduce yourself here and ask questions](https://gitlab.com/polyvent/polyvent/-/issues/38).


## Using this site - issues and directory

### Reading issues

To see the list of all `issues`,
click on `Issues` tab in the left navigation panel (3rd icon from the top). 
You can `search`, `filter` or `sort` the `issues` by `labels`.

When you see any `issue` of interest, you can enter and add your `comments` at the bottom, 
or below each `comment` or `comment thread`.


### Email notifications
You can subscribe to `notifications` for any `issue` by `toggling` the `notification` switch
at the very bottom in the right navigation panel (you may need to `scroll down`).


### Open new issue

To start new `issue` go to the list of all `issues` as described above and click the green button `New issue`
in the right top corner.

You can simply add `title` and `description` (skipping all other fields), and click on the green `Submit issue` button.

### Files and directories
To see all `files` and `directories` stored on this site, click on `Repository` 
 tab in the left navigation panel (2nd icon from the top). 

### Formatting
For advanced formatting, `headers`, `lists`, `itemization`, `images`, `videos`, `mathematics formulas` and much more,
use the Markdown language https://docs.gitlab.com/ee/user/markdown.html

### PolyVent Makers at Global Innovation Field Trip
<div class="video-fallback">
  See the video: <a href="https://www.youtube.com/embed/JAx1xZrKnBs">PolyVent Makers</a>.
</div>
<figure class="video-container">
  <iframe src="https://www.youtube.com/embed/JAx1xZrKnBs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

