
//Nathaniel's vent pin mapping (in order) : oxygen1, oxygen2, air 1, air 2, out 1, out 2, patient_exp, nothing, fan1, fan2

const int permanent_pin_mapping [10] = {2, 3, 4, 5, 6, 7, 8, 9, 14, 15};
//states:
//0: first bellow pumps, second bellow refills
//1: patient breathes out
//2: second bellow pumps, first bellow refills
//3: patient breathes out
const int states_of_operation [4][7] {
  {0, 0, 0, 0, 1, 0, 0},
  {0, 0, 0, 1, 0, 0, 1},
  {0, 0, 0, 0, 0, 1, 0},
  {0, 0, 1, 0, 0, 0, 1}
};
void setup() {
  for (int x = 0; x < sizeof permanent_pin_mapping; x++) {
    pinMode(permanent_pin_mapping[x], OUTPUT);
    digitalWrite(permanent_pin_mapping[x], LOW);
  }
  digitalWrite(permanent_pin_mapping[8], HIGH);
  digitalWrite(permanent_pin_mapping[9], HIGH);
}

void loop() {
  if (digitalRead(10) == LOW) {
    for (int x = 0; x < 4; x++) {
      for (int y = 0; y < 7; y++) {
        digitalWrite(permanent_pin_mapping[y], states_of_operation[x][y]);
      }
      delay(2500);
    }
  }
}

//
//for(int valve_num = 0; valve_num < 10; valve_num++){
//      digitalWrite(permanent_pin_mapping[valve_num], values[valve_num]);
//      //Serial.println(values[valve_num]);
//   }
