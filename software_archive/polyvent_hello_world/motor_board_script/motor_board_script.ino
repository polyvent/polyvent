#include <AccelStepper.h>

// Define a stepper and the pins it will use
AccelStepper stepper1(1, 4, 5);
AccelStepper stepper2(1, 7, 8); 

const int mmperrev_bellow1 = 8, mmperrev_bellow2 = 8;
const int stepsperrev_bellow1 = 400, stepsperrev_bellow2 = 400;
const int bellow_displacement = 50; //should be 50 mm for proper timing, speeds and accels will have to be adjusted otherwise

void setup() {
  stepper1.setMaxSpeed(3333);
  stepper1.setAcceleration(13333);
  stepper2.setMaxSpeed(3333);
  stepper2.setAcceleration(13333);
  
  stepper2.moveTo(bellow_displacement * stepsperrev_bellow2/mmperrev_bellow2);
  while(stepper2.distanceToGo() > 0){
      stepper2.run();
  }
}

void loop() {
 if(digitalRead(10) == LOW){
    //Pumping the first bellow, the whole manoever will take 1 second, 0.25 seconds of accelerationm, 0.5 seconds of top speed coasting, 0.25 seconds of deceleration *this also raises the second bellow
    
    stepper1.setMaxSpeed(3333);
    stepper1.setAcceleration(13333); 
    stepper1.moveTo(bellow_displacement * stepsperrev_bellow1/mmperrev_bellow1);
    while(stepper1.distanceToGo() > 0){
      stepper1.run();
    }
    delay(1500);
    
    stepper2.setMaxSpeed(1111);
    stepper2.setAcceleration(4444);
    stepper2.moveTo(0);
    while(stepper2.distanceToGo() < 0){
      stepper2.run();
    }

    stepper2.setMaxSpeed(3333);
    stepper2.setAcceleration(13333); 
    stepper2.moveTo(bellow_displacement * stepsperrev_bellow1/mmperrev_bellow1);
    while(stepper2.distanceToGo() > 0){
      stepper2.run();
    }
    delay(1500);

    stepper1.setMaxSpeed(1111);
    stepper1.setAcceleration(4444);
    stepper1.moveTo(0);
    while(stepper1.distanceToGo() < 0){
      stepper1.run();
    } 
 }
}
