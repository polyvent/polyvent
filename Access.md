## Access to this site
This site runs on the [`Gitlab platform`](https://gitlab.com/).
User access is managed by `Gitlab`.
Initial registration can be made with username/password or using `Google/Twitter/Github` or other sites.


### Invitation by email
If you registered following `email invitation`, 
next time to access the site,
make sure to `sign in` to `Gitlab` with the same method as in your initial registration.

E.g. if you registered with `username/password` but later `register/sign in` with `Google/Twitter` etc, 
you will be treated as different user, unless both `sign in` methods are connected.


### Self-registration with Gitlab
If you have `self-registered` with `Gitlab` and been added to this site by admin,
next time to access the site,
make sure to `sign in` to `Gitlab` as the same user.


### Connect your login methods
Connect all your `sign in` methods on your profile page
https://gitlab.com/profile/account
and use any of them in the future,
so `signing in` with another method or
`forgetting password` won't be a problem.
